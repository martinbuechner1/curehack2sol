sap.ui.define([
	"sap/ui/core/mvc/Controller",
	'sap/ui/core/HTML',
	"sap/ui/model/json/JSONModel"
], function (Controller, JSONModel, HTML) {
	"use strict";
	var HomepageController = Controller.extend("curehack2sol.controller.dashboard", {

		/**
		 * Called when a controller is instantiated and its View controls (if available) are already created.
		 * Can be used to modify the View before it is displayed, to bind event handlers and do other one-time initialization.
		 * @memberOf curehack2sol.view.dashboard
		 */
		akku_ges_model: new JSONModel(),
		toLogin: function () {
			this.router.navTo("login");
		},
		toMediathek: function () {
			this.router.navTo("mediathek");
		},
		onInit: function () {

			var that = this;
			this.router = this.getOwnerComponent().getRouter();
			//this.getView().setModel(that.akku_ges_model, 'akku_ges');
			/*that.akku_ges_model
				.loadData(
					"/backend/Things('59C38344DC1A4A8EBCCACF6467D31DFC')/hack2sol.team7.curemannheimhack2sol:Akku_ges/akku_ges?$top=100&timerange=7D"
				);*/
			var onboard_view = this.getView().byId("onboard_view");
			var video = this.getView().byId("video");
			/*setInterval(function () {
				}, 1000);*/
			onboard_view.setContent('<iframe width="500" height="315" src="https://www.youtube.com/embed/vILKN74z5-Y?autoplay=1" frameborder="0" allow="accelerometer; autoplay; encrypted-media; gyroscope; picture-in-picture; loop" ></iframe>');
			video.setContent('<iframe width="470" height="315" src="https://www.youtube.com/embed/zQeJlxmj48Y?loop=1" frameborder="0" allow="accelerometer; autoplay; encrypted-media; gyroscope; picture-in-picture" allowfullscreen></iframe>')
		}

		/**
		 * Similar to onAfterRendering, but this hook is invoked before the controller's View is re-rendered
		 * (NOT before the first rendering! onInit() is used for that one!).
		 * @memberOf curehack2sol.view.dashboard
		 */
		//	onBeforeRendering: function() {
		//
		//	},

		/**
		 * Called when the View has been rendered (so its HTML is part of the document). Post-rendering manipulations of the HTML could be done here.
		 * This hook is the same one that SAPUI5 controls get after being rendered.
		 * @memberOf curehack2sol.view.dashboard
		 */
		//	onAfterRendering: function() {
		//
		//	},

		/**
		 * Called when the Controller is destroyed. Use this one to free resources and finalize activities.
		 * @memberOf curehack2sol.view.dashboard
		 */
		//	onExit: function() {
		//
		//	}

	});
	return HomepageController;
});