sap.ui.define([
	'jquery.sap.global',
	'sap/ui/core/Fragment',
	'sap/ui/core/mvc/Controller',
	'sap/ui/model/json/JSONModel',
	'sap/m/Popover',
	'sap/m/Button'
], function (jQuery, Fragment, Controller, JSONModel, Popover, Button) {
	"use strict";

	function compare(a, b) {
		if (a.name.split('_').length > 0 && a.name.split('_').length > 0) {
			if (parseInt(a.name.split('_')[1], 10) < parseInt(b.name.split('_')[1], 10))
				return -1;
			if (parseInt(a.name.split('_')[1], 10) > parseInt(b.name.split('_')[1], 10))
				return 1;
			return 0;
		} else {
			return 0;
		}
	}

	var CController = Controller.extend("curehack2sol.controller.main", {
		model: new sap.ui.model.json.JSONModel(),
		data: {
			selectedKey: 'dashboard',
			navigation: [{
				title: 'Dashboard',
				icon: 'sap-icon://bbyd-dashboard',
				key: 'dashboard'
			}, {
				title: 'Akku',
				icon: 'sap-icon://vehicle-repair',
				key: 'akku'
			}, {
				title: 'Vehicle Dynamics',
				icon: 'sap-icon://bus-public-transport',
				key: 'vd'
			}, {
				title: 'vital',
				icon: 'sap-icon://customer',
				key: 'vital'
			}]
		},
		tv: {
			torque: [{
				tv: '-1'
			}]
		},
		zellen_temp: new JSONModel(),
		zellen_spannung: new JSONModel(),
		leistung: new JSONModel(),
		peri: new JSONModel(),
		onInit: function () {
			this.model.setData(this.data);
			this.getView().setModel(this.model);
			this.router = this.getOwnerComponent().getRouter();
			this.router.attachRoutePatternMatched();

			var that = this;
			this.leistung.setData(this.drehzahl);
			this.getView().setModel(that.leistung, 'leistung');
			that.leistung
				.loadData(
					"/backend/Things('59C38344DC1A4A8EBCCACF6467D31DFC')/hack2sol.team7.curemannheimhack2sol:Akku_ges/akku_ges?$top=100&timerange=7D"
				);

			var container = this.getView().byId('page');
			container.destroyContent();
			var oFragment = sap.ui.xmlfragment('curehack2sol.view.dashboard');
			container.addContent(oFragment);

			// this..setData(this.drehzahl);
			var that = this;
			this.getView().setModel(that.zellen_temp, 'temperatur');

			that.zellen_temp.attachRequestCompleted(function () {
				var iotaeData = that.zellen_temp.getData();
				var data = {
					value: []
				};
				if (iotaeData.value.length > 0) {
					var values = iotaeData.value[0];
					Object.keys(values).forEach(function (key) {
						var name = key;
						var temp = values[key];
						data.value.push({
							name: name,
							temp: temp
						});
					});
					data.value = data.value.sort(compare);
					that.zellen_temp.setData(data);
				}
			});
			that.zellen_temp
				.loadData(
					"/backend/Things('DC3C3B8DBEF946F8BDF77D1725083594')/hack2sol.team7.curemannheimhack2sol:Akku_Details/akkuzellen_temperatur?$top=1&timerange=7D"
				);
			that.zellen_spannung.attachRequestCompleted(function () {
				var iotaeData = that.zellen_spannung.getData();
				var data = {
					value: []
				};
				if (iotaeData.value.length > 0) {
					var values = iotaeData.value[0];
					Object.keys(values).forEach(function (key) {
						var name = key;
						var spannung = values[key];
						data.value.push({
							name: name,
							spannung: spannung
						});
					});
					data.value = data.value.sort(compare);
					that.zellen_spannung.setData(data);
				}
			});
			this.getView().setModel(that.zellen_spannung, 'spannung');
			that.zellen_spannung
				.loadData(
					"/backend/Things('DC3C3B8DBEF946F8BDF77D1725083594')/hack2sol.team7.curemannheimhack2sol:Akku_Details/akkuzellen_spannung?$top=1&timerange=7D"
				);
		},

		onItemSelect: function (oEvent) {
			var item = oEvent.getParameter('item');
			var container = this.getView().byId('page');
			container.destroyContent();
			var oFragment = sap.ui.xmlfragment('curehack2sol.view.' + item.getKey());
			container.addContent(oFragment);
		},

		onSideNavButtonPress: function () {
			var viewId = this.getView().getId();
			var toolPage = sap.ui.getCore().byId(viewId + "--toolPage");
			var sideExpanded = toolPage.getSideExpanded();

			this._setToggleButtonTooltip(sideExpanded);

			toolPage.setSideExpanded(!toolPage.getSideExpanded());
		},
		_setToggleButtonTooltip: function (bLarge) {
			var toggleButton = this.byId('sideNavigationToggleButton');
			if (bLarge) {
				toggleButton.setTooltip('Large Size Navigation');
			} else {
				toggleButton.setTooltip('Small Size Navigation');
			}
		}

	});

	return CController;

});