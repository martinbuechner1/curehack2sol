sap.ui.define([
	"sap/ui/core/mvc/Controller",
	
    "sap/ui/model/json/JSONModel"
], function (Controller) {
	"use strict";
	var mediathekController = Controller.extend("curehack2sol.controller.mediathek", {

		/**
		 * Called when a controller is instantiated and its View controls (if available) are already created.
		 * Can be used to modify the View before it is displayed, to bind event handlers and do other one-time initialization.
		 * @memberOf curehack2sol.view.dashboard
		 */
		toHomepage: function(){
			this.router.navTo("homepage");
		},
		onInit: function () {
			this.router = this.getOwnerComponent().getRouter();
			/*setInterval(function () {
			
			}, 1000);*/

		}

		/**
		 * Similar to onAfterRendering, but this hook is invoked before the controller's View is re-rendered
		 * (NOT before the first rendering! onInit() is used for that one!).
		 * @memberOf curehack2sol.view.dashboard
		 */
		//	onBeforeRendering: function() {
		//
		//	},

		/**
		 * Called when the View has been rendered (so its HTML is part of the document). Post-rendering manipulations of the HTML could be done here.
		 * This hook is the same one that SAPUI5 controls get after being rendered.
		 * @memberOf curehack2sol.view.dashboard
		 */
		//	onAfterRendering: function() {
		//
		//	},

		/**
		 * Called when the Controller is destroyed. Use this one to free resources and finalize activities.
		 * @memberOf curehack2sol.view.dashboard
		 */
		//	onExit: function() {
		//
		//	}

	});
	return mediathekController;
});